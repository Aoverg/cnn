# Using CNN to learn handwriting
In this project we make a program that uses Convolutional Neural Network to learn handwritten letters from A-F in capital form.
The program takes inn a folder with several letters from A-F and creat a convolutional base that we compile. after we have compiled the data the program then trains 10 times using data learned from previous runst to get a better understanding and get better at reconising the letters.
After compleating the training the result is then plotted to a pyplot with hits and missrate per run so you can see how it gets better over time.

## What you need to run the program
The program uses python, tensorflow, numpy and pathlib.

python you can download from its home page at : https://www.python.org/downloads/ and because numpy and pathlib is standard library you only need to use pip install on tensorflow with
```
python pip -m install tensorflow
```

## To run the system
To run the system ou simply run the program using command:

Windows:
```
python .\cnn.py
```

Linux:
```
python cnn.py
```

# Results
During the testing i chose to only go with 20 epochs so that we can easier see the results of how diffrent amount of filters change the outcome. Below you can see images from using 16, (16 and 32), (16, 32 and 64) and one with only 64 filters and how that changed the outcome.

20 epochs and 16 filters:
![20 epochs and 16 filters](img/epo20-16filters.png)

20 epochs with 32 filters on top of 16:
![20 epochs with 16 and 32 filters](img/epo20-32filters.png)

20 epochs with 64 filters on top of 32 and 16:
![20 epochs with 64, 32 and 16 filters](img/epo20-64filters.png)

20 epochs with only 64 filters alone:
![20 epochs with 64 filters](img/epo20-only64filters.png)

## Contributors
Alexander Øvergård